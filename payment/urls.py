from django.conf.urls import url
from django.urls import path
from .views import *
app_name='payment'
urlpatterns = [
    path(r'payment_info/', payment_info, name ="payment_info"),
    path(r'payment_detail/<str:pk>/', payment_detail, name ="payment_detail"),
    path(r'checkout/', new_checkout, name ="new_checkout"),
    path(r'new_checkout_process/', new_checkout_process, name ="new_checkout_process"),
]