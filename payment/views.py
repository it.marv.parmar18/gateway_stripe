from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
import stripe
from gateway_stripe.config import get_stripe_public_key, get_stripe_secret_key
from .models import *
from .stripe import *
from datetime import datetime
import json
# Create your views here.
#stripe listen --forward-to http://localhost:8000/webhook/
STRIPE_SECRET_KEY = "sk_test_51IUDDcIX7H1lFVv1OSjZt7ia68q4pNvMet2RX9eVqi2DHYhV4rZoEcuSDm5MZjyAHR92qk8JRHNdpyTC6EqtZ9ak00DXWemVG8"
@csrf_exempt
def new_checkout(request):
	context = {}
	return render(request, 'new_payment.html', context=context)

@csrf_exempt
def new_checkout_process(request):
	context = {}
	
	# total_price >fetched and calculated from backend table {tax/promotion etc...}
	total_price = 1099.9999999
	amount = int(total_price * 100)
	#quantity fetched from Table: default 1
	quantity = 1

	# fetch plan name from Table
	plan_name = "Premium" 

	stripe.api_key = STRIPE_SECRET_KEY
	checkout_session = stripe.checkout.Session.create(
		payment_method_types=['card','bacs_debit'],
		line_items=[
			{
				'price_data': {
					'currency': 'gbp',
					'unit_amount': amount,
					'product_data': {
						'name': plan_name,
					},
				},
				'quantity': quantity,
			},
			
		],
		payment_intent_data = {
			'setup_future_usage' : 'off_session'
		},
		mode='payment',
		success_url='http://18.132.233.54:8000/subscription',
		cancel_url='http://18.132.233.54:8000/subscription',
	)
	#send 200OK with session id to front end for checkout
	context["session_id"] = checkout_session.id
	context["STRIPE_PUBLISHABLE_KEY"] = get_stripe_public_key()
	return render(request, 'checkout.html', context=context)
	
def payment_token(request):
	token = request.POST.get("token",None)
	payment_type = request.POST.get("payment_type",None)
	#total_price = price * quantity
	#amount = total_price * 100 {for stripe purpose}
	total_price = 75 * 2
	amount = total_price * 100 #{for stripe purpose}
	
	if token and payment_type:
		stripe.api_key = get_stripe_secret_key()
		if payment_type == "card":
			intent = stripe.PaymentIntent.create(
				amount = amount,
				currency = "gbp",
				payment_method_data=dict(
					type="card",
					card=dict(
						token=token
					)
				), 
			)
		elif payment_type == "redirect":
			intent = stripe.PaymentIntent.create(
				amount = amount,
				currency = "gbp",
				payment_method_data=dict(
					type="bacs_debit",
					bacs_debit =dict(
						token=token
					)
				), 
			)

		


def payment_info(request):
	context = {}
	subscriber_obj = Subcription_Info_Model.objects.all()
	context["subscriber_obj"]= subscriber_obj
	return render(request, 'payment_info.html', context = context)

def payment_detail(request, pk):
	context = {}
	subscriber_obj = Subcription_Info_Model.objects.get(Customer_ID = pk)
	#subscriber_obj = Subcription_Info_Model.objects.filter(Customer_ID = pk).update(Payment_Status = "Succeed")
	context['subscriber'] = subscriber_obj
	return render(request, 'payment_detail.html', context = context)