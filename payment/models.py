from django.db import models
from django.db.models.signals import pre_save
from .stripe import create_product_plan
from django.dispatch import receiver

class Subscription_Plan(models.Model):
	Plan_Name 			= models.CharField(max_length = 30)
	Price 				= models.IntegerField()

	#This Can be stored thrugh Post Signal 
	Product_Plan_ID 	= models.CharField(max_length = 30,null = True, blank = True)
	Product_Price_ID 	= models.CharField(max_length = 30,null = True, blank = True)

	def __str__(self):
		return self.Plan_Name

	# class Payment_Process_Table(models.Model):

 # method for updating

def subscription_pre_save_receiver(sender, instance, **kwargs):
	instance.Product_Plan_ID, instance.Product_Price_ID =  create_product_plan(plan_price = instance.Price, product_name = instance.Plan_Name)
	

pre_save.connect(subscription_pre_save_receiver, sender=Subscription_Plan)

payment_type_choices = [
	("bacs_debit", "bacs_debit"),
	("card","card"),
	("invoice","invoice")
]
payment_status_choices = [
	("In-Progress", "In-Progress"),
	("Succeed","Succeed"),
	("Failed","Failed")
]

class Subcription_Info_Model(models.Model):
	Customer_ID 	= models.CharField(max_length = 30)
	Customer_Email 	= models.EmailField(max_length = 255, unique =True)
	Subscription_ID = models.CharField(max_length = 30, null=True, blank = True)
	is_subscription = models.BooleanField(default = False)
	Invoice_ID = models.CharField(max_length = 30, null=True, blank = True)
	Payment_Type = models.CharField(max_length = 30, choices = payment_type_choices, null=True, blank=True)
	End_Date 		= models.DateTimeField(null = True, blank = True)
	Subscription_Plan =  models.ForeignKey(Subscription_Plan, on_delete=models.CASCADE, null=True, blank=True)
	Payment_Status = models.CharField(max_length = 50,choices= payment_status_choices, default = "In-Progress", null=True, blank=True)
	Payment_Failed_Code = models.CharField(max_length = 50, null=True, blank=True)
	Payment_Failed_Reason = models.CharField(max_length = 50, null=True, blank=True)
	def __str__(self):
		return self.Customer_Email
	
	def is_subsciption(self):
		return self.is_subsciption

	
	def end_date(self):
		return self.End_Date
	
	


