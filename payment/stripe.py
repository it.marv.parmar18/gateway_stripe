import stripe
from gateway_stripe.config import get_stripe_public_key, get_stripe_secret_key
from .models import *

def create_product_plan(plan_price, product_name):
	stripe.api_key = get_stripe_secret_key()
	product_obj = stripe.Product.create(name=product_name)

	price_obj = stripe.Price.create(
		  unit_amount=plan_price * 100,
		  currency="gbp",
		  recurring={"interval": "year"},
		  product=product_obj.id,
		)
	return product_obj.id, price_obj.id


def retrieve_customer(customer_id):
        stripe.api_key = get_stripe_secret_key()
        customer_obj = stripe.Customer.retrieve(customer_id)
        return customer_obj

def retrieve_subscription(subscrption_id):
        stripe.api_key = get_stripe_secret_key()
        subscription_obj = stripe.Subscription.retrieve(subscrption_id)
        return subscription_obj

def createCustomer(customer_email, payment_method_id):
        stripe.api_key = get_stripe_secret_key()
        customer_obj = stripe.Customer.create(
                email = customer_email,
                payment_method = payment_method_id,
                invoice_settings={"default_payment_method": payment_method_id},
        )
        return customer_obj

def createSubscription(customer_id, price_id):
        stripe.api_key = get_stripe_secret_key()
        subscription_obj = stripe.Subscription.create(
                customer = customer_id,
                items = [
                        {
                                'price' : price_id
                        }
                ]
        )
        return subscription_obj

def modify_confirmPaymentIntent(payment_intent_id, payment_method_id, customer_id):
        stripe.api_key = get_stripe_secret_key()
        # stripe.PaymentIntent.modify(
        #         payment_intent_id,
        #         payment_method = payment_method_id,
        #         customer = customer_id
        # )

	#Confirming the Payment
        payment_intent_obj = stripe.PaymentIntent.confirm(
                payment_intent_id,
        )
        return payment_intent_obj

def retrievePaymentIntent(payment_intent_id):
        stripe.api_key = get_stripe_secret_key()
        payment_intent_obj = stripe.PaymentIntent.retrieve(
			payment_intent_id
	)
        return payment_intent_obj

def retrieveInvoice(invoice_id):
        stripe.api_key = get_stripe_secret_key()
        invoice_obj = stripe.Invoice.retrieve(
		invoice_id,
	)
        return invoice_obj

def sendInvoiceSubscription(subscription_plan, email):
        stripe.api_key = get_stripe_secret_key()
        customer_obj = stripe.Customer.create(
                email = email,
        )
        subscription_obj = stripe.Subscription.create(
                customer = customer_obj.id,
                items = [
                        {
                                'price' : subscription_plan.Product_Price_ID,
                                
                        }             
                ],
                collection_method='send_invoice',
                days_until_due=30,
        )

        return customer_obj, subscription_obj

def createSubscription_debit(customer_id, price_id, payment_method):
        stripe.api_key = get_stripe_secret_key()
        subscription_obj = stripe.Subscription.create(
                customer=customer_id,
                default_payment_method=payment_method,
                items=[{
                        'price':price_id,
                }],
        )
        return subscription_obj

def getSessionPaymentMethod(session_id):
        stripe.api_key = get_stripe_secret_key()
        session = stripe.checkout.Session.retrieve(
                id = session_id,
                expand=['setup_intent'],
        )
        return session.setup_intent.payment_method
"""
def delprice():
    list= []
    pl = stripe.Plan.list(limit= 100)
    for i in range(0, len(pl.data)):
            list.append(pl.data[i].id)
    for l in list:
            stripe.Plan.delete(l)
    list = []
    pl = stripe.Product.list(limit= 100)
    for i in range(0, len(pl.data)):
            list.append(pl.data[i].id)
    for l in list:
            stripe.Product.delete(l)
    print("DONE")
"""