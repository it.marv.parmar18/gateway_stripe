from django.contrib import admin

from .models import Subcription_Info_Model, Subcription_Info_Model,Subscription_Plan
# Register your models here.
admin.site.register(Subcription_Info_Model)

admin.site.register(Subscription_Plan)